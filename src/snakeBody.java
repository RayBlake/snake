import java.awt.*;
import java.util.Random;
import java.util.Timer;

public class snakeBody {
    private final int DELAY = 140; //speed of game
    private final int SIZE = 10; //size of head & body
    private final int ALL = 333;

    private int snakeBody;

    private final int x[] = new int[ALL];
    private final int y[] = new int[ALL];

    private final int randBait = 29;
    private final int baitSize = 10;

    private int bait_x;
    private int bait_y;


    private Image ball;
    private Image bait;
    private Image head;


    private void snake() {
        snakeBody = 3;

        for (int i = 0; i < snakeBody; i++) {
            x[i] = 50 - i * 10;
            y[i] = 50;
        }
        locateBait();
    }

    private void locateBait(){
        Random rand = new Random();
        int r = rand.nextInt(20);
        bait_x = ((r*SIZE));
        int t = rand.nextInt(20);
        bait_y = ((t*SIZE));
    }

    private void checkBait(){
        if ((x[0] == bait_x)  && (y[0]) == bait_y){
            snakeBody++;
            locateBait();
        }
    }




}
