import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;

import java.awt.*;
//<<<<<<< Updated upstream
import java.util.Random;
//=======
import java.util.Timer;
//>>>>>>> Stashed changes

import static org.lwjgl.glfw.GLFW.*;

public class Main {



	public Main() {
		GLFWErrorCallback errorCallback;
		glfwSetErrorCallback(errorCallback = GLFWErrorCallback.createPrint(System.err));
		if(!glfwInit())
			throw new IllegalStateException("Unable to initialize GLFW");
		String title = "MyTitle"; // The title of the window, WARNING, if title is
		// null, the code will segfault at glfwCreateWindow()

		int m_width = 640; // width of the window
		int m_height = 480; // height of the window

		glfwDefaultWindowHints(); // Loads GLFW's default window settings
		glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE); // Sets window to be visible
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // Sets whether the window is resizable

		long id = glfwCreateWindow(m_width, m_height, title, 0, 0); // Does the actual window creation
		if(id == 0)
			throw new RuntimeException("Failed to create window");
		Input.setWindowID(id);

		glfwMakeContextCurrent(id); // glfwSwapInterval needs a context on the calling thread, otherwise will cause NO_CURRENT_CONTEXT error
		GL.createCapabilities(); // Will let lwjgl know we want to use this context as the context to draw with

		glfwSwapInterval(1); // How many draws to swap the buffer
		glfwShowWindow(id); // Shows the window

		glfwPollEvents();

		Game.start();
		boolean playing = true;
		while(playing) {
			glfwPollEvents();
			Game.update();
			try {
				Thread.sleep(50);
			}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}


	public static void main(String[] args) {
		new Main();
	}
}