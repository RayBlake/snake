import java.util.Random;

import static org.lwjgl.glfw.GLFW.*;





public class Input {
	private static long window;
	public static void setWindowID(long id) { window = id; }

	public static boolean leftIsPressed() { return (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS); }

	public static boolean upIsPressed() {
		return (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS);
	}

	public static boolean rightIsPressed() {
		return (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS);
	}

	public static boolean downIsPressed() {
		return (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS);
	}




}

